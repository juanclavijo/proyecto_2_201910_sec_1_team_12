package model.data_structures;



import data_structures.LinearProbingHash;
import junit.framework.TestCase;


public class TestLinearProbingHash extends TestCase
{

	private LinearProbingHash<Integer, String> tabla = new LinearProbingHash<Integer, String>() ;

	/**
	 * Arreglo con los elementos del escenario
	 */
	protected static final String[] ADRESS_ID = { "350", "383", "105", "233", "140", "266", "356", "236", "80", "360",
			"221", "241", "130", "244", "352", "446", "18", "98", "97" };
	protected static final Integer[] LLAVES = { 1,2,3,4,5,6,7,8,9,11,12,13,14,15,16,17,18,19,20 };
	

	public void setupEscenario1() {

		for (int i = 0; i < ADRESS_ID.length; i++) {
			
			String valueAct = ADRESS_ID [i];
			int keyAct = LLAVES[i];
		
			tabla.put(keyAct, valueAct);
			
		}

	}

	public void testAdd() throws Exception {
		// Prueba la lista vac�a.
		assertTrue("La tabla debe estar vacia", tabla.isEmpty());
		assertEquals("no es 0", 0, tabla.size());

		// Agrega dos elementos.
		
	
		tabla.put(91384398, "nuevo");
		tabla.put(45349549, "holi");
		assertFalse("La lista no deberia estar vacia", tabla.isEmpty());
		assertEquals("Debe contener 2 elementos", 2, tabla.size());

		// Agrega 20 elementos.
		tabla.delete(91384398);
		tabla.delete(45349549);
		setupEscenario1();

		assertFalse("La lista no es vacia", tabla.isEmpty());
		assertEquals("La lista debe tener 20 elementos", ADRESS_ID.length, tabla.size());

	}
	

	public void testDelete() throws Exception {
		
				// Prueba la lista vac�a.
				assertTrue("La tabla debe estar vacia", tabla.isEmpty());
				assertEquals("no es 0", 0, tabla.size());

				// Agrega dos elementos.
				
			
				tabla.put(91384398, "nuevo");
				tabla.put(45349549, "holi");
				assertFalse("La lista no deberia estar vacia", tabla.isEmpty());
				assertEquals("Debe contener 2 elementos", 2, tabla.size());

				// Elimina los 2 elementos
				
				tabla.delete(91384398);
				tabla.delete(45349549);
				assertTrue("La tabla debe estar vacia", tabla.isEmpty());
				
				//Carga los 20 elementos 
				setupEscenario1();
				
	}
	
	public void testContains() throws Exception {
		
		// Prueba la lista vac�a.
		assertTrue("La tabla debe estar vacia", tabla.isEmpty());
		assertEquals("no es 0", 0, tabla.size());

		// Agrega dos elementos.
		
	
		tabla.put(91384398, "nuevo");
		tabla.put(45349549, "holi");
		assertFalse("La lista no deberia estar vacia", tabla.isEmpty());
		assertEquals("Debe contener 2 elementos", 2, tabla.size());

		// Elimina los 2 elementos
		
		assertTrue("La tabla debe contener la llave dada", tabla.contains(91384398));
		assertFalse("La tabla  NO debe contener la llave dada", tabla.contains(91386698));
		
		tabla.put(123456, "nuevo2");
		assertEquals(true, tabla.contains(123456));
		
		//Carga los 20 elementos 
		setupEscenario1();
		
}
public void testGet() throws Exception {
		
		// Prueba la lista vac�a.
		assertTrue("La tabla debe estar vacia", tabla.isEmpty());
		assertEquals("no es 0", 0, tabla.size());

		// Agrega dos elementos.
		
	
		tabla.put(91384398, "nuevo");
		tabla.put(45349549, "holi");
		assertFalse("La lista no deberia estar vacia", tabla.isEmpty());
		assertEquals("Debe contener 2 elementos", 2, tabla.size());

		// Elimina los 2 elementos
		
		assertEquals("nuevo", tabla.get(91384398));
		assertEquals("holi", tabla.get(45349549));
		assertNull(tabla.get(123456789));
		
}	
	
}
