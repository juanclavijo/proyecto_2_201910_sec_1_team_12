package model.data_structures;

import static org.junit.Assert.*;

//import org.junit.jupiter.api.Test;

import junit.framework.TestCase;
import model.util.Sort;
import vo.VOMovingViolations;

public class SortTest extends TestCase
{
	
	VOMovingViolations[] arreglo = new VOMovingViolations[20];
	
	protected static final String[] OBJECTID = { "14476381", "14476382", "14476383", "14476384", "14476385", "14476386",
			"14476387", "14476388", "14476389", "14476390", "14476391", "14476392", "14476393", "14476394", "14476395",
			"14476396", "14476397", "14476398", "14476399", "1447700" };

	protected static final String[] LOCATION = { "1400 BLK S CAPITOL ST SE N/B", "100 BLK MICHIGAN AVE NW E/B",
			"1900 BLK BRANCH AVE SE S/B", "2200 BLOCK K ST NW E/B", "800 BLK RIDGE RD  SE NW/B",
			"2500 BLK N CAPITOL ST NE N/B", "600 BLK KENILWORTH AVE NE S/B", "DC295 SW .7 MILES S/O EXIT 1 S/B",
			"DC295 NE .1MILE S/O EASTERN AVE S/B", "600 BLK KENILWORTH AVE NE S/B", "N CAPITOL ST N/B @ HAREWOOD RD NE",
			"600 BLK KENILWORTH AVE NE S/B", "3RD ST TUNNEL NW S/B BY 3RD ST", "3900 BLK SOUTH DAKOTA AVE NE SE/B",
			"600 BLK KENILWORTH AVE NE S/B", "800 BLK RIDGE RD  SE NW/B", "200 BLK RIGGS RD NE E/B",
			"3RD ST TUNNEL NW S/B BY 3RD ST", "600 BLK NEW YORK AVENUE NE W/B", "601 BLK NEW YORK AVENUE NE W/B" };

	protected static final String[] ADDRESS_ID = { "277954", "815694", "800638", "805065", "289024", "802253", "813891",
			"810381", "804032", "813891", "807916", "813891", "807662", "806707", "813891", "289024", "300679",
			"807662", "288753", "288759" };

	protected static final String[] STREETSEGID = { "1405", "675", "4361", "1280", "1870", "11963", "6993", "5134",
			"11963", "6783", "11963", "6364", "5751", "11963", "1280", "6946", "6364", "14658", "303", "365" };

	protected static final String[] XCOORD = { "399257.7", "398728.3", "403477.898", "395664.92", "404923.72",
			"399216.2", "404478.8329", "398406.31", "405635.9275", "404478.8329", "399303.5142", "404478.8329",
			"398778.9239", "401900.38", "404478.8329", "404923.72", "399697.56", "398778.9239", "400381.05", "400381.05" };

	protected static final String[] YCOORD = { "133847.29", "139835.9", "133227.7484", "137186.89", "134434.84",
			"139448.49", "136788.8974", "127737.13", "138164.7691", "136788.8974", "141609.9565", "136788.8974",
			"136745.5746", "141072.64", "136788.8974", "134434.84", "143158.72", "136745.5746", "138273.43", "400381.05" };

	protected static final String[] TICKETTYPE = { "Moving", "Moving", "Moving", "Moving", "Moving", "Moving", "Moving",
			"Moving", "Moving", "Moving", "Moving", "Moving", "Moving", "Moving", "Moving", "Moving", "Moving",
			"Moving", "Moving", "Moving" };

	protected static final String[] FINEAMT = { "300", "100", "100", "150", "100", "100", "300", "100", "100", "100",
			"150", "200", "150", "100", "100", "100", "100", "100", "100", "100"};

	protected static final String[] TOTALPAID = { "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0",
			"0", "0", "200", "0", "0", "0" };

	protected static final String[] PENALTY1 = { "300", "100", "100", "150", "100", "100", "300", "100", "100", "100",
			"150", "200", "150", "100", "100", "100", "100", "100", "100", "100" };

	protected static final String[] PENALTY2 = { "300", "100", "100", "150", "100", "100", "300", "100", "100", "100",
			"150", "200", "150", "100", "100", "100", "100", "100", "100", "100" };

	protected static final String[] ACCIDENTINDICATOR = { "No", "No", "No", "No", "No", "No", "No", "No", "No", "No",
			"No", "No", "No", "No", "No", "No", "No", "No", "No", "No" };

	protected static final String[] TICKETISSUEDATE = { "2018-04-01T11:59:00.000Z", "2018-04-01T00:35:00.000Z",
			"2018-04-01T04:40:00.000Z", "2018-04-01T14:47:00.000Z", "2018-04-01T15:05:00.000Z",
			"2018-04-01T13:42:00.000Z", "2018-04-01T17:14:00.000Z", "2018-04-01T13:10:00.000Z",
			"2018-04-01T11:52:00.000Z", "2018-04-01T16:38:00.000Z", "2018-04-01T05:21:00.000Z",
			"2018-04-01T15:06:00.000Z", "2018-04-01T13:44:00.000Z", "2018-04-01T17:04:00.000Z",
			"2018-04-01T15:08:00.000Z", "2018-04-01T00:50:00.000Z", "2018-04-01T19:11:00.000Z",
			"2018-04-01T23:13:00.000Z", "2018-04-01T01:36:00.000Z", "2018-04-01T01:36:00.000Z" };

	protected static final String[] VIOLATIONCODE = { "T122", "T119", "T119", "T120", "T119", "T119", "T122", "T119",
			"T119", "T119", "T113", "T121", "T120", "T119", "T119", "T119", "T119", "T119", "T119", "T119" };

	protected static final String[] VIOLATIONDESC = { "SPEED 26-30 MPH OVER THE SPEED LIMIT",
			"SPEED 11-15 MPH OVER THE SPEED LIMIT", "SPEED 11-15 MPH OVER THE SPEED LIMIT",
			"SPEED 16-20 MPH OVER THE SPEED LIMIT", "SPEED 11-15 MPH OVER THE SPEED LIMIT",
			"SPEED 11-15 MPH OVER THE SPEED LIMIT", "SPEED 26-30 MPH OVER THE SPEED LIMIT",
			"SPEED 11-15 MPH OVER THE SPEED LIMIT", "SPEED 11-15 MPH OVER THE SPEED LIMIT",
			"SPEED 11-15 MPH OVER THE SPEED LIMIT", "FAIL TO STOP PER REGULATIONS FACING RED SIGNAL",
			"SPEED 21-25 MPH OVER THE SPEED LIMIT", "SPEED 16-20 MPH OVER THE SPEED LIMIT",
			"SPEED 11-15 MPH OVER THE SPEED LIMIT", "SPEED 11-15 MPH OVER THE SPEED LIMIT",
			"SPEED 11-15 MPH OVER THE SPEED LIMIT", "SPEED 11-15 MPH OVER THE SPEED LIMIT",
			"SPEED 11-15 MPH OVER THE SPEED LIMIT", "SPEED 11-15 MPH OVER THE SPEED LIMIT","SPEED 11-15 MPH OVER THE SPEED LIMIT" };

	public void setupEscenario1() {
		for (int i = 0; i < OBJECTID.length; i++) {
			VOMovingViolations nuevo = new VOMovingViolations(OBJECTID[i], LOCATION[i], ADDRESS_ID[i], STREETSEGID[i],
					XCOORD[i], YCOORD[i], TICKETTYPE[i], FINEAMT[i], TOTALPAID[i], PENALTY1[i], PENALTY2[i],
					ACCIDENTINDICATOR[i], TICKETISSUEDATE[i], VIOLATIONCODE[i], VIOLATIONDESC[i]);
			
			arreglo[i] = nuevo;
		}

	}
		
	// Muestra de datos a ordenar
	
	public final static String VIOLATIONDESCR = "violationdescription";

	


	
	public void testMergeSort1() 
	{
		
		setupEscenario1();
		
		Sort.ordenarMergeSort(VIOLATIONDESCR,true,arreglo);
		
		boolean isSorted = false;
		for(int i = 1; i < arreglo.length ;i++)
			if(arreglo[i].compareTo(arreglo[i-1]) < 0) isSorted = false;
		isSorted = true;
		assertEquals("No se orden� correctamente",true, isSorted);
	}

	
	//@Test
	public void testQuickSort1() 
	{
		
		setupEscenario1();
		
		Sort.ordenarMergeSort(VIOLATIONDESCR,true,arreglo);
		
		boolean isSorted;
		for(int i = 1; i < arreglo.length ;i++)
			if(arreglo[i].compareTo(arreglo[i-1]) < 0) isSorted = false;
		isSorted = true;
		assertEquals("No se orden� correctamente",true, isSorted);
	}
	
	
	//@Test
	public void testShellSort1() 
	{
		
		setupEscenario1();
		
		
		Sort.ordenarMergeSort(VIOLATIONDESCR,true,arreglo);
		
		boolean isSorted;
		for(int i = 1; i < arreglo.length;i++)
			if(arreglo[i].compareTo(arreglo[i-1]) < 0) isSorted = false;
		isSorted = true;
		assertEquals("No se orden� correctamente",true, isSorted);
	}
	


}
