package model.util;

import data_structures.IArregloDinamico;
import logic.ManejoFechaHora;
import vo.VOMovingViolations;

/**************************
 * Compilation: javac Shell.java Execution: java Shell < input.txt Dependencies:
 * StdOut.java StdIn.java Data files:
 * https://algs4.cs.princeton.edu/21elementary/tiny.txt
 * https://algs4.cs.princeton.edu/21elementary/words3.txt
 * 
 * Sorts a sequence of strings from standard input using shellsort.
 *
 * Uses increment sequence proposed by Sedgewick and Incerpi. The nth element of
 * the sequence is the smallest integer >= 2.5^n that is relatively prime to all
 * previous terms in the sequence. For example, incs[4] is 41 because 2.5^4 =
 * 39.0625 and 41 is the next integer that is relatively prime to 3, 7, and 16.
 * 
 * % more tiny.txt S O R T E X A M P L E
 *
 * % java Shell < tiny.txt A E E L M O P R S T X [ one string per line ]
 * 
 * % more words3.txt bed bug dad yes zoo ... all bad yet
 * 
 * % java Shell < words3.txt all bad bed bug dad ... yes yet zoo [ one string
 * per line ]
 *
 *
 **************************/

public class Sort {

	public final static String VIOLATIONDESCR = "violationdescription";
	public final static String STREETID = "streetid";
	public final static String TICKETISSUEDATE = "ticketissuedate";
	public final static String XCOORD = "xcoord";
	public final static String YCOORD = "ycoord";
	public final static String FECHA = "fecha";

	/**
	 * Ordenar datos aplicando el algoritmo ShellSort
	 *
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos
	 *              ordenados (final)
	 */
	public static void ordenarShellSort(String criterio, boolean ascendente, VOMovingViolations[] datos) {
		int n = datos.length;

		// 3x+1 increment sequence: 1, 4, 13, 40, 121, 364, 1093, ...
		int h = 1;
		while (h < n / 3) {
			h = 3 * h + 1;
		}

		while (h >= 1) {
			// h-sort the array
			for (int i = h; i < n; i++) {
				for (int j = i; j >= h && less(criterio, ascendente, datos[j], datos[j - h]); j -= h) {
					exchange(datos, j, j - h);
				}
			}

			h /= 3;
		}
		// TODO implementar el algoritmo ShellSort
	}

	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 *
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos
	 *              ordenados (final)
	 */
	public static void ordenarMergeSort(String criterio, boolean ascendente, VOMovingViolations[] datos) {
		VOMovingViolations[] aux = new VOMovingViolations[datos.length];
		sortMerge(criterio, ascendente, datos, aux, 0, datos.length - 1);
		assert isSorted(criterio, ascendente, datos);
		// TODO implementar el algoritmo MergeSort
	}

	private static void sortMerge(String criterio, boolean ascendente, VOMovingViolations[] a, VOMovingViolations[] aux,
			int lo, int hi) {
		if (hi <= lo) {
			return;
		}
		int mid = lo + (hi - lo) / 2;
		sortMerge(criterio, ascendente, a, aux, lo, mid);
		sortMerge(criterio, ascendente, a, aux, mid + 1, hi);
		merge(criterio, ascendente, a, aux, lo, mid, hi);
	}

	private static void merge(String criterio, boolean ascendente, VOMovingViolations[] a, VOMovingViolations[] aux,
			int lo, int mid, int hi) {
		// precondition: a[lo .. mid] and a[mid+1 .. hi] are sorted subarrays
		assert isSorted(criterio, ascendente, a, lo, mid);
		assert isSorted(criterio, ascendente, a, mid + 1, hi);

		// copy to aux[]
		for (int k = lo; k <= hi; k++) {
			aux[k] = a[k];
		}

		// merge back to a[]
		int i = lo, j = mid + 1;
		for (int k = lo; k <= hi; k++) {
			if (i > mid) {
				a[k] = aux[j++];
			} else if (j > hi) {
				a[k] = aux[i++];
			} else if (less(criterio, ascendente, aux[j], aux[i])) {
				a[k] = aux[j++];
			} else {
				a[k] = aux[i++];
			}
		}

		// postcondition: a[lo .. hi] is sorted
		assert isSorted(criterio, ascendente, a, lo, hi);
	}

	/**
	 * Ordenar datos aplicando el algoritmo QuickSort
	 *
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos
	 *              ordenados (final)
	 */
	public static void ordenarQuickSort(String criterio, boolean ascendente, VOMovingViolations[] datos) {

		shuffle(datos);
		sortQuick(criterio, ascendente, datos, 0, datos.length - 1);
		assert isSorted(criterio, ascendente, datos);

		// TODO implementar el algoritmo QuickSort
	}

	public static void shuffle(VOMovingViolations[] arreglo) {
		int n = arreglo.length;

		for (int i = 0; i < n; i++) {
			int ran = i + (int) (Math.random() * (n - i));

			VOMovingViolations temp = arreglo[ran];
			arreglo[ran] = arreglo[0];
			arreglo[0] = temp;
		}
	}

	private static void sortQuick(String criterio, boolean ascendente, VOMovingViolations[] a, int lo, int hi) {
		if (hi <= lo) {
			return;
		}
		int j = partition(criterio, ascendente, a, lo, hi);
		sortQuick(criterio, ascendente, a, lo, j - 1);
		sortQuick(criterio, ascendente, a, j + 1, hi);
		assert isSorted(criterio, ascendente, a, lo, hi);
	}

	private static int partition(String criterio, boolean ascendente, VOMovingViolations[] a, int lo, int hi) {
		int i = lo;
		int j = hi + 1;
		VOMovingViolations v = a[lo];
		while (true) {

			// find item on lo to swap
			while (less(criterio, ascendente, a[++i], v)) {
				if (i == hi) {
					break;
				}
			}

			// find item on hi to swap
			while (less(criterio, ascendente, v, a[--j])) {
				if (j == lo) {
					break; // redundant since a[lo] acts as sentinel
				}
			}

			// check if pointers cross
			if (i >= j) {
				break;
			}

			exchange(a, i, j);
		}

		// put partitioning item v at a[j]
		exchange(a, lo, j);

		// now, a[lo .. j-1] <= a[j] <= a[j+1 .. hi]
		return j;
	}

	/**
	 * Comparar 2 objetos usando la comparacion "natural" de su clase
	 * 
	 * @param v primer objeto de comparacion
	 * @param w segundo objeto de comparacion
	 * @return true si v es menor que w usando el metodo compareTo. false en caso
	 *         contrario.
	 */
	private static boolean less(String criterioComparacion, boolean ascendente, VOMovingViolations v,
			VOMovingViolations w) {

		boolean RTA = false;

		if (ascendente == true) {

			switch (criterioComparacion) {

			case TICKETISSUEDATE:
				if (v.getTicketIssueDate().compareTo(w.getTicketIssueDate()) < 0) {
					RTA = true;
				}
				break;

			case STREETID:
				if (v.getStreetSegid() < w.getStreetSegid()) {
					RTA = true;
				}
				break;

			case VIOLATIONDESCR:
				if (v.getViolationDesc().compareTo(w.getViolationDesc()) < 0) {
					RTA = true;
				}
				break;
			case XCOORD:
				if (v.getxCOORD() < w.getxCOORD()) {
					RTA = true;
				}
				break;
			case YCOORD:
				if (v.getyCOORD() < w.getyCOORD()) {
					RTA = true;
				}
				break;
			case FECHA:
				if (ManejoFechaHora.convertirFecha_Hora_LDT(v.getTicketIssueDate())
						.compareTo(ManejoFechaHora.convertirFecha_Hora_LDT(w.getTicketIssueDate())) < 0) {
					RTA = true;
				}
				break;
			}

		} else {

			switch (criterioComparacion) {

			case TICKETISSUEDATE:
				if (v.getTicketIssueDate().compareTo(w.getTicketIssueDate()) > 0) {
					RTA = true;
				}
				break;

			case STREETID:
				if (v.getStreetSegid() > w.getStreetSegid()) {
					RTA = true;
				}
				break;

			case VIOLATIONDESCR:
				if (v.getViolationDesc().compareTo(w.getViolationDesc()) > 0) {
					RTA = true;
				}
				break;

			case XCOORD:
				if (v.getxCOORD() > w.getxCOORD()) {
					RTA = true;
				}
				break;
			case YCOORD:
				if (v.getyCOORD() > w.getyCOORD()) {
					RTA = true;
				}
				break;
			case FECHA:
				if (ManejoFechaHora.convertirFecha_Hora_LDT(v.getTicketIssueDate())
						.compareTo(ManejoFechaHora.convertirFecha_Hora_LDT(w.getTicketIssueDate())) > 0) {
					RTA = true;
				}
				break;
			}
		}

		return RTA;
	}

	/**
	 * Intercambiar los datos de las posicion i y j
	 *
	 * @param datos contenedor de datos
	 * @param i     posicion del 1er elemento a intercambiar
	 * @param j     posicion del 2o elemento a intercambiar
	 */
	private static void exchange(VOMovingViolations[] datos, int i, int j) {
		VOMovingViolations temporal = datos[i];
		datos[i] = datos[j];
		datos[j] = temporal;
		// TODO implementar
	}

	private static boolean isSorted(String criterio, boolean ascendente, VOMovingViolations[] a) {
		return isSorted(criterio, ascendente, a, 0, a.length - 1);
	}

	private static boolean isSorted(String criterio, boolean ascendente, VOMovingViolations[] a, int lo, int hi) {
		for (int i = lo + 1; i <= hi; i++) {
			if (less(criterio, ascendente, a[i], a[i - 1])) {
				return false;
			}
		}
		return true;
	}

}