
package controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;

import data_structures.ArregloDinamico;
import data_structures.IArregloDinamico;
import data_structures.IQueue;
import data_structures.LinearProbingHash;
import data_structures.MaxHeapCP;
import data_structures.Queue;
import data_structures.RedBlackBalancedSearchTrees;
import data_structures.linkedList;
import logic.MovingViolationsManager;
import logic.ManejoFechaHora;
import model.util.Sort;
import vo.EstadisticasCargaInfracciones;
import vo.InfraccionesFecha;
import vo.InfraccionesFechaHora;
import vo.InfraccionesFranjaHoraria;
import vo.InfraccionesFranjaHorariaViolationCode;
import vo.InfraccionesLocalizacion;
import vo.InfraccionesViolationCode;
import vo.VOMovingViolations;
import view.MovingViolationsManagerView;

public class Controller {

	// Componente vista (consola)
	private MovingViolationsManagerView view;

	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */


	private IQueue<VOMovingViolations> colaList = new Queue<VOMovingViolations>();

	// Componente modelo (logica de la aplicacion)
	private MovingViolationsManager manager;



	public final static String VIOLATIONDESCR = "violationdescription";
	public final static String STREETID = "streetid";
	public final static String TICKETISSUEDATE = "ticketissuedate";
	public final static String XCOORD = "xcoord";
	public final static String YCOORD = "ycoord";
	public final static String FECHA = "fecha";

	public Controller() 
	{
		view = new MovingViolationsManagerView();
		manager = new MovingViolationsManager();
		colaList = manager.darListaColaMovingViolations();
	}

	public void run() 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin = false;
		Controller controller = new Controller();

		long startTime;
		long endTime;
		long duration;

		int[] datos;
		double[] coordenadas;

		while (!fin) {
			view.printMenu();

			int option = sc.nextInt();

			switch (option) {
			case 0:
				view.printMessage("Ingrese el semestre (1 o 2)");
				int semestre = sc.nextInt();
				startTime = System.currentTimeMillis();
				datos = controller.loadMovingViolations(semestre);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo de carga: " + duration + " milisegundos");
				if(semestre == 1)
				{
					view.printMessage("Se cargaron " + datos[0]+ " infracciones de enero");
					view.printMessage("Se cargaron " + datos[1]+ " infracciones de febrero");
					view.printMessage("Se cargaron " + datos[2]+ " infracciones de marzo");
					view.printMessage("Se cargaron " + datos[3]+ " infracciones de abril");
					view.printMessage("Se cargaron " + datos[4]+ " infracciones de mayo");
					view.printMessage("Se cargaron " + datos[5]+ " infracciones de junio");
				}
				else
				{
					view.printMessage("Se cargaron " + datos[0]+ " infracciones de julio");
					view.printMessage("Se cargaron " + datos[1]+ " infracciones de agosto");
					view.printMessage("Se cargaron " + datos[2]+ " infracciones de septiembre");
					view.printMessage("Se cargaron " + datos[3]+ " infracciones de octubre");
					view.printMessage("Se cargaron " + datos[4]+ " infracciones de noviembre");
					view.printMessage("Se cargaron " + datos[5]+ " infracciones de diciembre");
				}
				view.printMessage("Se cargaron un total de: " + controller.colaList.size() + " infracciones en el semestre.");
				coordenadas = controller.zonaGeograficaMinmax();
				view.printMessage("La zona geografica Minmax tiene coordenadas en: "
						+ "("+coordenadas[0]+","+coordenadas[1]+") ("+coordenadas[2]+","+coordenadas[3]+")");


				//EstadisticasCargaInfracciones resumenCarga = manager.loadMovingViolations(semestre);

				//TODO Mostrar resultado de tipo EstadisticasCargaInfracciones con: 
				//     total de infracciones cargadas, numero de infracciones cargadas por mes y zona Minimax (Xmin, Ymin) y (Xmax, Ymax)
				//view.printResumenLoadMovingViolations( ... );

				break;

			case 1:
				view.printMessage("1A. Consultar las N franjas horarias con mas infracciones que desea ver. Ingresar valor de N: ");
				int numeroFranjas = sc.nextInt();

				IQueue<InfraccionesFranjaHoraria> RTA = new Queue<InfraccionesFranjaHoraria>();

				IQueue<VOMovingViolations> copia = controller.colaList;

				RTA = rankingNFranjas(numeroFranjas, copia);

				view.printReq1A(RTA);
				break;

			case 2:
				view.printMessage("Ingrese la coordenada en X de la localizacion geografica (Ej. 393185,8): ");
				double xcoord = sc.nextDouble();
				view.printMessage("Ingrese la coordenada en Y de la localizacion geografica (Ej. 138316,9): ");
				double ycoord = sc.nextDouble();


				// TODO Completar para la invocacin del metodo 2B
				startTime = System.currentTimeMillis();
				InfraccionesLocalizacion A2 = controller.consultarPorLocalizacionHash(xcoord, ycoord);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo : " + duration + " milisegundos");

				// TODO Mostrar resultado de tipo InfraccionesLocalizacion
				view.printReq2A(A2);
				break;

			case 3:
				view.printMessage("Ingrese la fecha inicial del rango. Formato ao-mes-dia (ej. 2008-06-21)");
				String fechaInicialStr = sc.next();
				LocalDate fechaInicial = ManejoFechaHora.convertirFecha_LD(fechaInicialStr);

				view.printMessage("Ingrese la fecha final del rango. Formato ao-mes-dia (ej. 2008-06-30)");
				String fechaFinalStr = sc.next();
				LocalDate fechaFinal = ManejoFechaHora.convertirFecha_LD(fechaFinalStr);

				IQueue<VOMovingViolations> copia2 = controller.colaList;

				Queue<InfraccionesFecha> resultados = new Queue<InfraccionesFecha>();

				resultados = consultarInfraccionesPorRangoFechas(fechaInicial, fechaFinal, copia2);

				view.printReq3A(resultados);
				break;


			case 4:
				view.printMessage("1B. Consultar los N Tipos con mas infracciones. Ingrese el valor de N: ");
				int numeroTipos = sc.nextInt();

				//TODO Completar para la invocaci�n del metodo 1B

				IQueue<InfraccionesViolationCode> resp4 =  new Queue<InfraccionesViolationCode>();
				startTime = System.currentTimeMillis();
				resp4 = controller.rankingNViolationCodes(numeroTipos);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo : " + duration + " milisegundos");

				//TODO Mostrar resultado de tipo Cola con N InfraccionesViolationCode
				view.printReq1B(resp4);
				break;

			case 5:						
				view.printMessage("Ingrese la coordenada en X de la localizacion geografica (Ej. 393185,8): ");
				xcoord = sc.nextDouble();
				view.printMessage("Ingrese la coordenada en Y de la localizacion geografica (Ej. 138316,9): ");
				ycoord = sc.nextDouble();

				//TODO Completar para la invocaci�n del metodo 2B
				startTime = System.currentTimeMillis();
				InfraccionesLocalizacion t = controller.consultarPorLocalizacionArbol(xcoord, ycoord);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo : " + duration + " milisegundos");

				//TODO Mostrar resultado de tipo InfraccionesLocalizacion 
				view.printReq2B(t);
				break;

			case 6:
				view.printMessage("Ingrese la cantidad minima de dinero que deben acumular las infracciones en sus rangos de fecha  (Ej. 393185,8)");
				double cantidadMinima = sc.nextDouble();

				view.printMessage("Ingrese la cantidad maxima de dinero que deben acumular las infracciones en sus rangos de fecha (Ej. 138316,9)");
				double cantidadMaxima = sc.nextDouble();

				IQueue<InfraccionesFechaHora> r = new Queue<InfraccionesFechaHora>();
				//TODO Completar para la invocaci�n del metodo 3B
				startTime = System.currentTimeMillis();
				r = controller.consultarFranjasAcumuladoEnRango(cantidadMinima, cantidadMaxima);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo : " + duration + " milisegundos");

				//TODO Mostrar resultado de tipo Cola con InfraccionesFechaHora 
				view.printReq3B(r);
				break;

			case 7:
				view.printMessage("1C. Consultar las infracciones con Address_Id. Ingresar el valor de Address_Id: ");
				int addressID = sc.nextInt();

				startTime = System.currentTimeMillis();
				//TODO Completar para la invocaci�n del metodo 1C

				InfraccionesLocalizacion rta = controller.consultarPorAddressId(addressID);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo : " + duration + " milisegundos");

				duration = endTime - startTime;
				view.printMessage("Tiempo requerimiento 1C: " + duration + " milisegundos");

				//TODO Mostrar resultado de tipo InfraccionesLocalizacion 	
				view.printReq1C(rta);
				break;

			case 8:
				view.printMessage("Ingrese la hora inicial del rango. Formato HH:MM:SS (ej. 09:30:00.000Z)");
				String horaInicialStr = sc.next();
				LocalTime horaInicial = ManejoFechaHora.convertirHora_LT(horaInicialStr);

				view.printMessage("Ingrese la hora final del rango. Formato HH:MM:SS (ej. 16:00:00.000Z)");
				String horaFinalStr = sc.next();
				LocalTime horaFinal = ManejoFechaHora.convertirHora_LT(horaFinalStr);

				startTime = System.currentTimeMillis();
				//TODO Completar para la invocacion del metodo 2C
				InfraccionesFranjaHorariaViolationCode ot = controller.consultarPorRangoHoras(horaInicial, horaFinal);

				endTime = System.currentTimeMillis();

				duration = endTime - startTime;
				view.printMessage("Tiempo requerimiento 2C: " + duration + " milisegundos");
				//TODO Mostrar resultado de tipo InfraccionesFranjaHorarioViolationCode
				view.printReq2C(ot);
				break;

			case 9:
				view.printMessage("Consultar las N localizaciones geograficas con mas infracciones. Ingrese el valor de N: ");
				int numeroLocalizaciones = sc.nextInt();
				IQueue<InfraccionesLocalizacion> req9 = new Queue<InfraccionesLocalizacion>();
				startTime = System.currentTimeMillis();

				req9 =controller.rankingNLocalizaciones(numeroLocalizaciones);

				endTime = System.currentTimeMillis();

				duration = endTime - startTime;
				view.printMessage("Tiempo requerimiento 3C: " + duration + " milisegundos");
				//TODO Mostrar resultado de tipo Cola con InfraccionesLocalizacion
				view.printReq3C(req9);
				break;

			case 10:

				System.out.println("Grafica ASCII con la informacion de las infracciones por ViolationCode");

				startTime = System.currentTimeMillis();
				//TODO Completar para la invocacion del metodo 4C
				MaxHeapCP<InfraccionesViolationCode> resp10 = controller.ordenarCodigosPorNumeroInfracciones();

				//TODO Mostrar grafica a partir del resultado del metodo anterior
				view.printReq4C(resp10, controller.colaList.size());
				endTime = System.currentTimeMillis();

				duration = endTime - startTime;
				view.printMessage("Tiempo requerimiento 4C: " + duration + " milisegundos");
				break;

			case 11:	
				fin = true;
				sc.close();
				break;
			}
		}

	}

	public int[] loadMovingViolations(int num) 
	{
		int[] resp = new int[6];
		if (num == 1) 
		{
			resp[0] = manager.loadJanuary("./data/Moving_Violations_Issued_in_January_2018.csv");
			resp[1] = manager.loadFebruary("./data/Moving_Violations_Issued_in_February_2018.csv");
			resp[2] = manager.loadMarch("./data/Moving_Violations_Issued_in_March_2018.csv");
			resp[3] = manager.loadApril("./data/Moving_Violations_Issued_in_April_2018.csv");
			resp[4] = manager.loadMay("./data/Moving_Violations_Issued_in_May_2018.csv");
			//resp[5] = manager.loadJune("./data/Moving_Violations_Issued_in_June_2018.csv");
		} 
		else if (num == 2) 
		{
			resp[0] = manager.loadJuly("./data/Moving_Violations_Issued_in_July_2018.csv");
			resp[1] = manager.loadAugust("./data/Moving_Violations_Issued_in_August_2018.csv");
			resp[2] = manager.loadSeptember("./data/Moving_Violations_Issued_in_September_2018.csv");
			resp[3] = manager.loadOctober("./data/Moving_Violations_Issued_in_October_2018.csv");
			resp[4] = manager.loadNovember("./data/Moving_Violations_Issued_in_November_2018.csv");
			resp[5] = manager.loadDecember("./data/Moving_Violations_Issued_in_December_2018.csv");

		} 
		else System.out.println("El semestre no es valido");

		return resp;
	}

	public double[] zonaGeograficaMinmax()
	{
		double[] resp = new double[4];
		double Xmin = 0;
		double Ymin = 0;
		double Xmax = 0;
		double Ymax = 0;

		ArregloDinamico paraOrdenar = new ArregloDinamico(colaList.size());
		for(VOMovingViolations a : colaList)
		{
			paraOrdenar.agregar(a);
		}

		Sort.ordenarMergeSort(XCOORD, true, paraOrdenar.darArreglo());
		Xmin = paraOrdenar.darElemento(0).getxCOORD();
		Xmax = paraOrdenar.darElemento(paraOrdenar.darTamano()-1).getxCOORD();

		Sort.ordenarMergeSort(YCOORD, true, paraOrdenar.darArreglo());
		Ymin = paraOrdenar.darElemento(0).getyCOORD();
		Ymax = paraOrdenar.darElemento(paraOrdenar.darTamano()-1).getyCOORD();

		resp[0] = Xmin;
		resp[1] = Ymin;
		resp[2] = Xmax;
		resp[3] = Ymax;
		return resp;
	}

	//---------------------------------------------------------------------------
	//---------------------------------------------------------------------------
	// REQUERIMIENTOS 
	//---------------------------------------------------------------------------
	//---------------------------------------------------------------------------


	/**
	 * Requerimiento 1A: Obtener el ranking de las N franjas horarias que tengan ms
	 * infracciones.
	 * 
	 * @param int N: Nmero de franjas horarias que tienen ms infracciones
	 * @return Cola con objetos InfraccionesFranjaHoraria
	 */
	public IQueue<InfraccionesFranjaHoraria> rankingNFranjas(int N, IQueue<VOMovingViolations> arregloParametro) {

		MaxHeapCP<InfraccionesFranjaHoraria> arregloGrande = new MaxHeapCP<InfraccionesFranjaHoraria>(23);

		Queue<InfraccionesFranjaHoraria> RTA = new Queue<InfraccionesFranjaHoraria>();

		Queue<VOMovingViolations> arregloActual = new Queue<VOMovingViolations>();

		IQueue<VOMovingViolations> copia =arregloParametro;


		ArregloDinamico paraOrdenar = new ArregloDinamico(copia.size());

		for (VOMovingViolations a : copia) {
			paraOrdenar.agregar(a);
		}

		Sort.ordenarMergeSort(FECHA, true, paraOrdenar.darArreglo());

		VOMovingViolations[] arreglo = new VOMovingViolations[copia.size()];

		for (int i = 0; i < arreglo.length; i++) {
			arreglo[i] = paraOrdenar.darElemento(i);
		}

		LocalTime horaReferencia = ManejoFechaHora.convertirFecha_Hora_LDT(arreglo[0].getTicketIssueDate())
				.toLocalTime();

		int contador = 0;

		for (int i = 0; i < arreglo.length; i++) {

			VOMovingViolations actual = arreglo[i];

			LocalTime LocalDateActual = ManejoFechaHora.convertirFecha_Hora_LDT(actual.getTicketIssueDate())
					.toLocalTime();


			if (horaReferencia.getHour() == LocalDateActual.getHour()) {
				arregloActual.enqueue(actual);
			}

			else {

				InfraccionesFranjaHoraria nuevo = new InfraccionesFranjaHoraria(horaReferencia, LocalDateActual,
						arregloActual);

				arregloGrande.insert(nuevo);

				arregloActual = null;

				arregloActual = new Queue<VOMovingViolations>();

				horaReferencia = LocalDateActual;

			}

		}

		for (int i = 0; i < N; i++) {
			RTA.enqueue(arregloGrande.delMax());
		}

		return RTA;
	}

	/**
	 * Requerimiento 2A: Consultar las infracciones por Localizacion Geogrfica
	 * (Xcoord, Ycoord) en Tabla Hash.
	 * 
	 * @param double xCoord : Coordenada X de la localizacion de la infraccin
	 *        double yCoord : Coordenada Y de la localizacion de la infraccin
	 * @return Objeto InfraccionesLocalizacion
	 */
	public InfraccionesLocalizacion consultarPorLocalizacionHash(double xCoord, double yCoord) {

		IQueue<VOMovingViolations> copia = new Queue<VOMovingViolations>();
		copia = manager.darCopiaQueue();

		ArregloDinamico paraOrdenar = new ArregloDinamico(copia.size());
		for (VOMovingViolations a : copia) {
			paraOrdenar.agregar(a);
		}

		Sort.ordenarMergeSort(XCOORD, true, paraOrdenar.darArreglo());
		Sort.ordenarMergeSort(YCOORD, true, paraOrdenar.darArreglo());

		VOMovingViolations[] arreglo = new VOMovingViolations[copia.size()];
		for (int i = 0; i < arreglo.length; i++) {
			arreglo[i] = paraOrdenar.darElemento(i);
		}


		LinearProbingHash<String, InfraccionesLocalizacion> tablaHash = new LinearProbingHash<String, InfraccionesLocalizacion>();

		for (int i = 0; i < arreglo.length; i++) {
			VOMovingViolations actual = arreglo[i];
			if (actual != null) {
				Queue<VOMovingViolations> value = new Queue<VOMovingViolations>();
				double keyXCoord = actual.getxCOORD();
				double KeyYCoord = actual.getyCOORD();
				value.enqueue(actual);

				for (int j = i + 1; j < arreglo.length; j++) {
					VOMovingViolations aj = arreglo[j];
					if ((aj != null) && (keyXCoord == aj.getxCOORD() && KeyYCoord == aj.getyCOORD())) {
						value.enqueue(aj);
						arreglo[j] = null;
					}
				}
				String llave = keyXCoord + ":" + KeyYCoord;
				InfraccionesLocalizacion nuevo = new InfraccionesLocalizacion(keyXCoord, KeyYCoord,
						actual.getLocation(), actual.getAdressId(), actual.getStreetSegid(), value);

				tablaHash.put(llave, nuevo);
			}
		}

		InfraccionesLocalizacion resp = null;
		boolean encontrado = false;
		String busqueda = xCoord + ":" + yCoord;
		while (!encontrado) {
			InfraccionesLocalizacion temp = tablaHash.get(busqueda);
			if (temp.getXcoord() == xCoord && temp.getYcoord() == yCoord) {
				resp = temp;
				encontrado = true;
			}
		}
		return resp;
	}

	/**
	 * Requerimiento 3A: Buscar las infracciones por rango de fechas
	 * 
	 * @param LocalDate fechaInicial: Fecha inicial del rango desqueda LocalDate
	 *                  fechaFinal: Fecha final del rango desqueda
	 * @return Cola con objetos InfraccionesFecha
	 */
	public Queue<InfraccionesFecha> consultarInfraccionesPorRangoFechas(LocalDate fechaInicial, LocalDate fechaFinal,
			IQueue<VOMovingViolations> arregloParametro) {

		Queue<InfraccionesFecha> RTA = new Queue<InfraccionesFecha>();

		IQueue<VOMovingViolations> copia = arregloParametro;

		Queue<VOMovingViolations> listaDelVOActual = new Queue<VOMovingViolations>();

		RedBlackBalancedSearchTrees<String, Queue<VOMovingViolations>> arbol = new RedBlackBalancedSearchTrees<>();

		ArregloDinamico paraOrdenar = new ArregloDinamico(copia.size());

		for (VOMovingViolations a : copia) {
			paraOrdenar.agregar(a);
		}

		Sort.ordenarMergeSort(FECHA, true, paraOrdenar.darArreglo());

		VOMovingViolations[] arreglo = new VOMovingViolations[copia.size()];

		for (int i = 0; i < arreglo.length; i++) {
			arreglo[i] = paraOrdenar.darElemento(i);

		}

		int mesInicial = fechaInicial.toString().charAt(7);

		int diaInicial = Integer.parseInt(fechaInicial.toString().substring(8, 10));

		int mesFinal = fechaFinal.toString().charAt(7);

		int diaFinal = Integer.parseInt(fechaFinal.toString().substring(8, 10));

		boolean cambio = false;

		String dateReferencia = null;

		Queue<VOMovingViolations> arregloActual = new Queue<VOMovingViolations>();

		for (int i = 0; i < arreglo.length; i++) {

			VOMovingViolations actual = arreglo[i];

			String DateAct = ManejoFechaHora.convertirFecha_Hora_LDT(actual.getTicketIssueDate()).toLocalDate()
					.toString();

			int mesActual = DateAct.charAt(7);

			int diaActual = Integer.parseInt(DateAct.substring(8, 10));

			System.out.println("inicial : " + ((mesInicial * 30) + diaInicial));

			System.out.println("actual  : " + ((mesActual * 30) + diaActual));

			System.out.println("final : " + ((mesFinal * 30) + diaFinal));

			if ((((mesInicial * 30) + diaInicial) <= ((mesActual * 30) + diaActual))
					&& (((mesActual * 30) + diaActual) <= ((mesFinal * 30) + diaFinal))) {

				if (cambio == false) {

					dateReferencia = ManejoFechaHora.convertirFecha_Hora_LDT(actual.getTicketIssueDate()).toLocalDate()
							.toString();
					cambio = true;
				}

				if (dateReferencia.equals(DateAct)) {

					arregloActual.enqueue(actual);
				} else if (!dateReferencia.equals(DateAct)) {
					cambio = false;
					arbol.put(dateReferencia, arregloActual);
					arregloActual = null;
					arregloActual = new Queue<VOMovingViolations>();
					arregloActual.enqueue(actual);
				}

			}

		}

		Iterator<String> it = arbol.keys().iterator();

		while (it.hasNext()) {

			String llaveActual = it.next();

			Queue<VOMovingViolations> deLlaveActual = arbol.get(llaveActual);

			InfraccionesFecha nuevo = new InfraccionesFecha(deLlaveActual, llaveActual);

			RTA.enqueue(nuevo);

		}

		return RTA;
	}




	/**
	 * Requerimiento 1B: Obtener  el  ranking  de  las  N  tipos  de  infracci�n
	 * (ViolationCode)  que  tengan  m�s infracciones.
	 * @param  int N: Numero de los tipos de ViolationCode con m�s infracciones.
	 * @return Cola con objetos InfraccionesViolationCode con top N infracciones
	 */
	public IQueue<InfraccionesViolationCode> rankingNViolationCodes(int N)
	{
		// TODO completar

		IQueue<VOMovingViolations> copia = new Queue<VOMovingViolations>();
		copia = manager.darCopiaQueue();

		VOMovingViolations[] arreglo = new VOMovingViolations[copia.size()];
		for(int i = 0; i < arreglo.length; i++)
		{
			arreglo[i] = copia.dequeue();
		}

		MaxHeapCP<InfraccionesViolationCode> prioridad =  new MaxHeapCP<InfraccionesViolationCode>(copia.size());

		for(int i = 0; i < arreglo.length; i++)
		{
			VOMovingViolations actual = arreglo[i];
			if(actual != null)
			{
				Queue<VOMovingViolations> value = new Queue<VOMovingViolations>();
				String keyActual = actual.getViolationCode();
				value.enqueue(actual);

				for(int j = i+1; j < arreglo.length; j++)
				{
					VOMovingViolations aj = arreglo[j];
					if(aj != null && keyActual.compareToIgnoreCase(aj.getViolationCode()) == 0)
					{
						value.enqueue(aj);
						arreglo[j] = null;
					}
				}

				InfraccionesViolationCode nuevo = new InfraccionesViolationCode(keyActual, value);
				prioridad.insert(nuevo);
			}

		}

		IQueue<InfraccionesViolationCode> resp = new Queue<InfraccionesViolationCode>();

		int contador = 1;
		while(contador <= N)
		{
			resp.enqueue(prioridad.delMax());
			contador++;
		}
		return resp;		
	}

	/**
	 * Requerimiento 2B: Consultar las  infracciones  por  
	 * Localizaci�n  Geogr�fica  (Xcoord, Ycoord) en Arbol.
	 * @param  double xCoord : Coordenada X de la localizacion de la infracci�n
	 *			double yCoord : Coordenada Y de la localizacion de la infracci�n
	 * @return Objeto InfraccionesLocalizacion
	 */
	public InfraccionesLocalizacion consultarPorLocalizacionArbol(double xCoord, double yCoord)
	{
		// TODO completar

		IQueue<VOMovingViolations> copia = new Queue<VOMovingViolations>();
		copia = manager.darCopiaQueue();

		ArregloDinamico paraOrdenar = new ArregloDinamico(copia.size());
		for(VOMovingViolations a : copia)
		{
			paraOrdenar.agregar(a);
		}

		Sort.ordenarMergeSort(XCOORD, true, paraOrdenar.darArreglo());
		Sort.ordenarMergeSort(YCOORD, true, paraOrdenar.darArreglo());

		VOMovingViolations[] arreglo = new VOMovingViolations[copia.size()];
		for(int i = 0; i < arreglo.length; i++)
		{	
			arreglo[i] = paraOrdenar.darElemento(i);
		}

		RedBlackBalancedSearchTrees <String, InfraccionesLocalizacion> arbol = new RedBlackBalancedSearchTrees<String, InfraccionesLocalizacion>();

		for(int i = 0; i<arreglo.length;i++)
		{
			VOMovingViolations actual = arreglo[i];
			if(actual != null)
			{
				Queue<VOMovingViolations> value = new Queue<VOMovingViolations>();
				double keyXCoord = actual.getxCOORD();
				double KeyYCoord = actual.getyCOORD();
				value.enqueue(actual);

				for(int j = i+1; j < arreglo.length; j++)
				{
					VOMovingViolations aj = arreglo[j];
					if((aj != null) &&(keyXCoord == aj.getxCOORD() && KeyYCoord == aj.getyCOORD()))
					{
						value.enqueue(aj);
						arreglo[j] = null;
					}
				}
				String llave = keyXCoord + ":" + KeyYCoord;
				InfraccionesLocalizacion nuevo = new InfraccionesLocalizacion(keyXCoord, KeyYCoord, 
						actual.getLocation(), actual.getAdressId(), actual.getStreetSegid(), value);
				arbol.put(llave, nuevo);
			}
		}

		String busqueda = xCoord + ":" + yCoord;
		InfraccionesLocalizacion resp = arbol.get(busqueda);
		return resp;		
	}

	/**
	 * Requerimiento 3B: Buscar las franjas de fecha-hora donde se tiene un valor acumulado
	 * de infracciones en un rango dado [US$ valor inicial, US$ valor final]. 
	 * @param  double valorInicial: Valor m�nimo acumulado de las infracciones
	 * 		double valorFinal: Valor m�ximo acumulado de las infracciones.
	 * @return Cola con objetos InfraccionesFechaHora
	 */
	public IQueue<InfraccionesFechaHora> consultarFranjasAcumuladoEnRango(double valorInicial, double valorFinal)
	{
		// TODO completar

		IQueue<VOMovingViolations> copia = new Queue<VOMovingViolations>();
		copia = manager.darCopiaQueue();

		VOMovingViolations[] arreglo = new VOMovingViolations[copia.size()];
		for(int i = 0; i < arreglo.length; i++)
		{
			arreglo[i] = copia.dequeue();
		}

		RedBlackBalancedSearchTrees <Double, InfraccionesFechaHora> arbol = new RedBlackBalancedSearchTrees<Double, InfraccionesFechaHora>();


		for(int i = 0; i < arreglo.length; i++)
		{
			VOMovingViolations act = arreglo[i];
			if(act != null)
			{
				double valorAcumuladoDeInfracciones = 0;
				IQueue<VOMovingViolations> temp = new Queue<VOMovingViolations>();
				String fechaCompleta = act.getTicketIssueDate();
				LocalDateTime fecha1 = ManejoFechaHora.convertirFecha_Hora_LDT(fechaCompleta);
				LocalDateTime fecha2 = null; 
				LocalDate date1 = ManejoFechaHora.convertirFecha_LD(fechaCompleta.split("T")[0]);
				LocalDate date2 = null; 
				temp.enqueue(act);
				for(int j = i+1; j < arreglo.length; j++)
				{
					VOMovingViolations act2 = arreglo[j];
					if(act2 != null)
					{
						String fechaCompleta2 = act.getTicketIssueDate();
						fecha2 = ManejoFechaHora.convertirFecha_Hora_LDT(fechaCompleta2);
						date2 = ManejoFechaHora.convertirFecha_LD(fechaCompleta2.split("T")[0]);
						if(date1.compareTo(date2) == 0)
						{
							temp.enqueue(act2);
							valorAcumuladoDeInfracciones += (act.getFineAMT() + act2.getFineAMT());
							arreglo[j] = null;
						}
					}
				}
				InfraccionesFechaHora nuevo = new InfraccionesFechaHora(fecha1, fecha2, temp);
				arbol.put(valorAcumuladoDeInfracciones, nuevo);
			}
		}

		Iterator<Double> iterator = arbol.keys(valorInicial, valorFinal).iterator();
		IQueue<InfraccionesFechaHora> resp = new Queue<InfraccionesFechaHora>();
		while(iterator.hasNext())
		{
			resp.enqueue(arbol.get(iterator.next()));
		}

		return resp;		
	}



	/**
	 * Requerimiento 1C: Obtener  la informaci�n de  una  addressId dada
	 * @param  int addressID: Localizaci�n de la consulta.
	 * @return Objeto InfraccionesLocalizacion
	 */
	public InfraccionesLocalizacion consultarPorAddressId(int addressID)
	{
		IQueue<VOMovingViolations> copia = new Queue<VOMovingViolations>();
		copia = manager.darCopiaQueue();

		VOMovingViolations[] arreglo = new VOMovingViolations[copia.size()];
		for(int i = 0; i < arreglo.length; i++)
		{
			arreglo[i] = copia.dequeue();
		}

		RedBlackBalancedSearchTrees <Double, InfraccionesLocalizacion> arbol = new RedBlackBalancedSearchTrees<Double, InfraccionesLocalizacion>();

		for(int i = 0; i < arreglo.length; i++)
		{
			VOMovingViolations actual = arreglo[i];
			if(actual != null)
			{
				Queue<VOMovingViolations> value = new Queue<VOMovingViolations>();
				double keyActual = actual.getAdressId();
				value.enqueue(actual);

				for(int j = i+1; j < arreglo.length; j++)
				{
					VOMovingViolations aj = arreglo[j];
					if(aj != null && keyActual == aj.getAdressId())
					{
						value.enqueue(aj);
						arreglo[j] = null;
					}
				}
				InfraccionesLocalizacion nuevo = new InfraccionesLocalizacion(actual.getxCOORD(), actual.getyCOORD(), 
						actual.getLocation(), keyActual, actual.getStreetSegid(), value);
				arbol.put(keyActual, nuevo);
			}
		}	
		return arbol.get((double)addressID);
	}

	/**
	 * Requerimiento 2C: Obtener  las infracciones  en  un  rango de
	 * horas  [HH:MM:SS  inicial,HH:MM:SS  final]
	 * @param  LocalTime horaInicial: Hora  inicial del rango de b�squeda
	 * 		LocalTime horaFinal: Hora final del rango de b�squeda
	 * @return Objeto InfraccionesFranjaHorariaViolationCode
	 */
	public InfraccionesFranjaHorariaViolationCode consultarPorRangoHoras(LocalTime horaInicial, LocalTime horaFinal)
	{
//		IQueue<VOMovingViolations> arregloParametro = new Queue<VOMovingViolations>();{
//
//			Queue<VOMovingViolations> lista = new Queue<VOMovingViolations>();
//
//			Queue<InfraccionesViolationCode> pInfViolationCode = new Queue<InfraccionesViolationCode>();
//
//			IQueue<VOMovingViolations> copia = arregloParametro;
//
//			ArregloDinamico paraOrdenar = new ArregloDinamico(copia.size());
//
//			for (VOMovingViolations a : copia) {
//				paraOrdenar.agregar(a);
//			}
//
//			Sort.ordenarMergeSort(FECHA, true, paraOrdenar.darArreglo());
//
//			VOMovingViolations[] arreglo = new VOMovingViolations[copia.size()];
//
//			for (int i = 0; i < arreglo.length; i++) {
//				arreglo[i] = paraOrdenar.darElemento(i);
//
//			}
//
//			int minutosIniciales = ((60 * horaInicial.getHour() + horaInicial.getMinute()));
//
//			int minutosFinales = ((60 * horaFinal.getHour() + horaFinal.getMinute()));
//
//			boolean cambio = false;
//
//			String dateReferencia = null;
//
//			Queue<VOMovingViolations> listaDelActual = new Queue<VOMovingViolations>();
//
//			for (int i = 0; i < arreglo.length; i++) {
//
//				VOMovingViolations actual = arreglo[i];
//
//				int minutosActual = ((ManejoFechaHora.convertirFecha_Hora_LDT(actual.getTicketIssueDate()).toLocalTime()
//						.getHour() * 60)
//						+ (ManejoFechaHora.convertirFecha_Hora_LDT(actual.getTicketIssueDate()).toLocalTime().getMinute()));
//
//				if (minutosIniciales <= minutosActual && minutosActual <= minutosFinales) {
//
//					lista.enqueue(actual);
//
//					if (cambio == false) {
//
//						dateReferencia = actual.getViolationCode();
//						cambio = true;
//					}
//
//					if (dateReferencia.equals(actual.getViolationCode())) {
//
//						listaDelActual.enqueue(actual);
//					}
//
//					else if (!dateReferencia.equals(actual.getViolationCode())) {
//
//						cambio = false;
//
//						InfraccionesViolationCode nuevo = new InfraccionesViolationCode(dateReferencia, listaDelActual);
//
//						pInfViolationCode.enqueue(nuevo);
//
//						listaDelActual = null;
//						listaDelActual = new Queue<VOMovingViolations>();
//						listaDelActual.enqueue(actual);
//					}
//
//				}
//
//			}
//
//			InfraccionesFranjaHorariaViolationCode RTA = new InfraccionesFranjaHorariaViolationCode(horaInicial, horaFinal,
//					lista, pInfViolationCode);
//		
//			return RTA;
//		}
//	
		
		
		
		IQueue<VOMovingViolations> lista1 = new Queue<VOMovingViolations>();
		IQueue<VOMovingViolations> temp = new Queue<VOMovingViolations>();
		IQueue<InfraccionesViolationCode> lista2 = new Queue<InfraccionesViolationCode>();

		RedBlackBalancedSearchTrees <LocalTime, IQueue<VOMovingViolations>> arbol = new RedBlackBalancedSearchTrees<LocalTime, IQueue<VOMovingViolations>>();

		IQueue<VOMovingViolations> copia = new Queue<VOMovingViolations>();
		copia = manager.darCopiaQueue();

		VOMovingViolations[] arreglo = new VOMovingViolations[copia.size()];
		for(int i = 0; i < arreglo.length; i++)
		{
			arreglo[i] = copia.dequeue();
		}

//////		for(int i = 0; i < arreglo.length; i++)
//////		{
//////			VOMovingViolations s = arreglo[i];
//////			if(s != null)
//////			{
//////				LocalTime time = ManejoFechaHora.convertirHora_LT(s.getTicketIssueDate().split("T")[1]);
//////				if(time.compareTo(horaInicial) >= 0 && time.compareTo(horaFinal) <= 0)
//////				{
//////					lista1.enqueue(s);
//////					temp.enqueue(s);
//////					for(int j = i+1; j < arreglo.length; j++)
//////					{
//////						VOMovingViolations s2 = arreglo[j];
//////						if(s2!=null)
//////						{
//////							LocalTime time2 = ManejoFechaHora.convertirHora_LT(s2.getTicketIssueDate().split("T")[1]);
//////							if(time.compareTo(time2)==0)
//////							{
//////								lista1.enqueue(s2);
//////								temp.enqueue(s);
//////								arreglo[j] = null;
//////							}
//////						}
//////					}
//////					arbol.put(time, lista1);
//////				}
//////			}
//////		}
		
		for(int i = 0; i < arreglo.length; i++)
		{
			VOMovingViolations s = arreglo[i];
			if(s != null)
			{
				LocalTime time = ManejoFechaHora.convertirHora_LT(s.getTicketIssueDate().split("T")[1]);
				if(time.compareTo(horaInicial) >= 0 && time.compareTo(horaFinal) <= 0)
				{
					lista1.enqueue(s);
					temp.enqueue(s);
				}
			}
		}

		VOMovingViolations[] subArreglo = new VOMovingViolations[temp.size()];
		for(int i = 0; i < subArreglo.length; i++)
		{
			subArreglo[i] = temp.dequeue();
		}

		//esto es lo que ordena los que encuentra por violation code
		for(int i = 0; i < subArreglo.length; i ++)
		{
			if(subArreglo[i] != null)
			{
				IQueue<VOMovingViolations> subTemp = new Queue<VOMovingViolations>();
				String vcActual = subArreglo[i].getViolationCode();
				for(int j = i+1; j < subArreglo.length; j++)
				{
					if((subArreglo[j] != null)&&(vcActual.compareTo(subArreglo[j].getViolationCode()) == 0))
					{
						subTemp.enqueue(subArreglo[j]);
						subArreglo[j] = null;				
					}
				}
				lista2.enqueue(new InfraccionesViolationCode(vcActual, subTemp));
			}

		}

		InfraccionesFranjaHorariaViolationCode resp = new InfraccionesFranjaHorariaViolationCode(horaInicial, horaFinal, lista1, lista2);
		return resp;


	}

	/**
	 * Requerimiento 3C: Obtener  el  ranking  de  las  N localizaciones geogr�ficas
	 * (Xcoord,  Ycoord)  con  la mayor  cantidad  de  infracciones.
	 * @param  int N: Numero de las localizaciones con mayor n�mero de infracciones
	 * @return Cola de objetos InfraccionesLocalizacion
	 */
	public IQueue<InfraccionesLocalizacion> rankingNLocalizaciones(int N)
	{
		IQueue<VOMovingViolations> copia = new Queue<VOMovingViolations>();
		copia = manager.darCopiaQueue();

		VOMovingViolations[] arreglo = new VOMovingViolations[copia.size()];
		for(int i = 0; i < arreglo.length; i++)
		{
			arreglo[i] = copia.dequeue();
		}


		MaxHeapCP<InfraccionesLocalizacion> prioridad =  new MaxHeapCP<InfraccionesLocalizacion>(copia.size());

		for(int i = 0; i<arreglo.length;i++)
		{
			VOMovingViolations actual = arreglo[i];
			if(actual != null)
			{
				Queue<VOMovingViolations> value = new Queue<VOMovingViolations>();
				double keyXCoord = actual.getxCOORD();
				double KeyYCoord = actual.getyCOORD();
				value.enqueue(actual);

				for(int j = i+1; j < arreglo.length; j++)
				{
					VOMovingViolations aj = arreglo[j];
					if((aj != null) &&(keyXCoord == aj.getxCOORD() && KeyYCoord == aj.getyCOORD()))
					{
						value.enqueue(aj);
						arreglo[j] = null;
					}
				}
				InfraccionesLocalizacion nuevo = new InfraccionesLocalizacion(keyXCoord, KeyYCoord, 
						actual.getLocation(), actual.getAdressId(), actual.getStreetSegid(), value);
				prioridad.insert(nuevo);
			}
		}

		IQueue<InfraccionesLocalizacion> resp = new Queue<InfraccionesLocalizacion>();

		int contador = 1;
		while(contador <= N)
		{
			resp.enqueue(prioridad.delMax());
			contador++;
		}

		return resp;			
	}

	/**
	 * Requerimiento 4C: Obtener la  informaci�n  de  los c�digos (ViolationCode) ordenados por su numero de infracciones.
	 * @return Contenedora de objetos InfraccionesViolationCode.
	  // TODO Definir la estructura Contenedora
	 */
	public MaxHeapCP<InfraccionesViolationCode> ordenarCodigosPorNumeroInfracciones()
	{
		// TODO completar
		// TODO Definir la Estructura Contenedora

		IQueue<VOMovingViolations> copia = new Queue<VOMovingViolations>();
		copia = manager.darCopiaQueue();

		VOMovingViolations[] arreglo = new VOMovingViolations[copia.size()];
		for(int i = 0; i < arreglo.length; i++)
		{
			arreglo[i] = copia.dequeue();
		}
		MaxHeapCP<InfraccionesViolationCode> resp =  new MaxHeapCP<InfraccionesViolationCode>(copia.size());

		for(int i = 0; i < arreglo.length; i++)
		{
			VOMovingViolations actual = arreglo[i];
			if(actual != null)
			{
				Queue<VOMovingViolations> value = new Queue<VOMovingViolations>();
				String keyActual = actual.getViolationCode();
				value.enqueue(actual);

				for(int j = i+1; j < arreglo.length; j++)
				{
					VOMovingViolations aj = arreglo[j];
					if(aj != null && keyActual.compareToIgnoreCase(aj.getViolationCode()) == 0)
					{
						value.enqueue(aj);
						arreglo[j] = null;
					}
				}

				InfraccionesViolationCode nuevo = new InfraccionesViolationCode(keyActual, value);
				resp.insert(nuevo);
			}

		}
		return resp;

	}


}
