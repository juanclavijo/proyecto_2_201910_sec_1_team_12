package data_structures;

import java.io.Serializable;

public class Node<T>  implements Serializable {


	/**
	 * Constante de serializaci�n
	 */
	private static final long serialVersionUID = 1L;
	

	/**
	 * Elemento almacenado en el nodo.
	 */
	
	private T actual;
	
	/**
	 * Siguiente nodo.
	 */
	 private Node<T> siguiente;
	 
		/**
		 * Nodo anterior.
		 */
		private Node<T> anterior;
	
	/**
	 * Constructor del nodo.
	 * @param elemento El elemento que se almacenar� en el nodo. elemento != null
	 */
	public Node(T elemento)
	{
		this.actual = elemento;
		
		this.siguiente = null;
		
		anterior= null;
	}
	
	
	public void cambiarSiguiente(Node<T> siguiente)
	{
		this.siguiente = siguiente;
	}
	

	public void cambiarAnterior(Node<T> anterior)
	{
		this.anterior = anterior;
	}

		public void cambiarElemento(T elemento)
	{
		this.actual= elemento;
	}
		
		
	public T darElemento()
	{
		return actual;
	}

	public Node<T> darSiguiente()
	{
		return siguiente;
	}

	public Node<T> darAnterior()
	{
		return anterior;
	}
	


}
