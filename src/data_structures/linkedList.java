package data_structures;

import java.util.Iterator;

	public class linkedList<T> implements ILinkedList<T> {
		/**
		 * Constante de serializaci�n.
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * Atributo que indica la cantidad de elementos que han sido almacenados en
		 * la lista.
		 */
		protected int cantidadElementos;

		/**
		 * Primer nodo de la lista.
		 */
		protected Node<T> primerNodo;

		/**
		 * Construye una lista vacia <b>post:< /b> se ha inicializado el primer nodo
		 * en null
		 */
		public linkedList() {
			primerNodo = null;
			cantidadElementos = 0;
		}

		/**
		 * 
		 * Devuelve un iterador sobre la lista El iterador empieza en el primer
		 * elemento
		 * 
		 * @return un nuevo iterador sobre la lista
		 */
		public Iterator<T> iterator() {
			return new iteratorLinkedList<T>(primerNodo);
		}

		/**
		 * Agrega un elemento al final de la lista Un elemento no se agrega si la
		 * lista ya tiene un elemento con el mismo id. Se actualiza la cantidad de
		 * elementos.
		 * 
		 * @param e
		 *            el elemento que se desea agregar.
		 * @return true en caso que se agregue el elemento o false en caso
		 *         contrario.
		 * @throws NullPointerException
		 *             si el elemento es nulo
		 */
		public void add(T elemento) {

			Node<T> nuevo = new Node<T>(elemento);

			if (elemento == null) {
				throw new NullPointerException("El elemento es null");
			}
			else {
				if (contains(elemento) == false) {

					if (primerNodo == null) {

						primerNodo = nuevo;
						cantidadElementos++;
					} else {

						primerNodo.cambiarAnterior(nuevo);
						nuevo.cambiarSiguiente(primerNodo);
						primerNodo = nuevo;
						cantidadElementos++;
					}
				}

			}

		}

		@Override
		public void addAtEnd(T elemento) {

			Node<T> nuevo = new Node<T>(elemento);
			if (primerNodo == null) {
				primerNodo = nuevo;
				cantidadElementos++;
			} else {
				Node<T> actual = primerNodo;
				while (actual.darSiguiente() != null) {
					actual = actual.darSiguiente();
				}
				actual.cambiarSiguiente(nuevo);
				nuevo.cambiarAnterior(actual);
				cantidadElementos++;
			}

		}

		@Override
		public void addAtK(T elemento, int posicion) {
			
			Node<T> nuevo = new Node<T>(elemento);

			if (posicion < 0 || posicion > cantidadElementos) {
				throw new IndexOutOfBoundsException("El index no cumple con las condiciones establecidas");
			} else if (elemento == null) {
				throw new NullPointerException("El elemento es nulo");
			} else {
				

					if (posicion == 0) {
						if (primerNodo == null) {
							primerNodo = nuevo;
							cantidadElementos++;
						} else {
							Node<T> p = (Node<T>) primerNodo;
							primerNodo = nuevo;
							nuevo.cambiarSiguiente(p);
							p.cambiarAnterior(nuevo);
							cantidadElementos++;
						}

					}
					else {
						int contador = 0;
						Node<T> actual = (Node<T>) primerNodo;
						Node<T> siguiente = (Node<T>) actual.darSiguiente();

						while (contador < posicion - 1) {
							actual = (Node<T>) actual.darSiguiente();
							siguiente = (Node<T>) actual.darSiguiente();
							contador++;
						}
						if (siguiente == null) {
							actual.cambiarSiguiente(nuevo);
							nuevo.cambiarAnterior(actual);
							cantidadElementos++;
						} else {
							nuevo.cambiarSiguiente(siguiente);
							actual.cambiarSiguiente(nuevo);
							siguiente.cambiarAnterior(nuevo);
							nuevo.cambiarAnterior(actual);
							cantidadElementos++;
						}
					}
				
			}

		}

		@Override
		public T get(int i) {

			if (i < 0 || i >= cantidadElementos) {
				throw new IndexOutOfBoundsException("Ha ocurrido un error, el indice es " + i
						+ "y el tama�o debera ser entre 0 y " + cantidadElementos);
			} else {
				int contador = 0;

				Node<T> Actual = primerNodo;

				while (contador < i && Actual != null) {
					Actual = Actual.darSiguiente();
					contador++;
				}
				return Actual.darElemento();
			}

		}

		@Override
		public T getCurrentElement() {
			return primerNodo.darElemento();
		}

		@Override
		public Integer getSize() {
			return cantidadElementos;
		}

		@Override
		public void delete() {
			if (primerNodo != null) {
				Node<T> sig = primerNodo.darSiguiente();

				primerNodo = sig;
				sig.cambiarAnterior(null);
				cantidadElementos--;
			}

		}

		@Override
		public void deleteAtK(int K) {

			if (K < 0 || K >= cantidadElementos) {
				throw new IndexOutOfBoundsException("La posicion no cumple con el rango esperado");
			} else if (primerNodo == null) {
				throw new IndexOutOfBoundsException("La lista está vacia!");
			} else {

				if (K == 0) {

					if (primerNodo.darSiguiente() != null) {

						primerNodo = primerNodo.darSiguiente();
						cantidadElementos--;
					} else {

						primerNodo.cambiarElemento(null);
						cantidadElementos--;
					}

				} else {
					int contador = 0;
					boolean eliminado = false;
					Node<T> actual = (Node<T>) primerNodo;
					Node<T> siguiente = (Node<T>) primerNodo.darSiguiente();

					while (eliminado == false && siguiente != null && contador <= K - 1) {

						if (contador == K - 1) {

							if (siguiente.darSiguiente() == null) {

								actual.cambiarSiguiente(null);
								cantidadElementos--;
								eliminado = true;
							}

							else {
								Node<T> sigsig = (Node<T>) siguiente.darSiguiente();
								actual.cambiarSiguiente(sigsig);
								sigsig.cambiarAnterior(actual);
								cantidadElementos--;
								eliminado = true;
							}

						}

						actual = (Node<T>) actual.darSiguiente();
						siguiente = (Node<T>) siguiente.darSiguiente();
						contador++;
					}

				}

			}

		}

		@Override
		public T next() {
			T RTA = null;

			if (primerNodo.darSiguiente() != null) {
				RTA = primerNodo.darSiguiente().darElemento();
			}
			return RTA;
		}

		@Override
		public T previous() {
			T RTA = null;

			if (primerNodo.darAnterior() != null) {
				RTA = primerNodo.darAnterior().darElemento();
			}
			return RTA;
		}

		public boolean contains(Object o) {

			boolean encontro = false;

			Node<T> Actualito = primerNodo;

			while (Actualito != null && !encontro) {
				if (Actualito.darElemento().equals(o)) {

					encontro = true;
				} else {
					Actualito = Actualito.darSiguiente();
				}
			}

			return encontro;

		}
	}
	
