package data_structures;

import vo.VOMovingViolations;

/**
 * 2019-01-23 Estructura de Datos Arreglo Dinamico de Strings. El arreglo al
 * llenarse (llegar a su maxima capacidad) debe aumentar su capacidad.
 * 
 * @author Fernando De la Rosa
 *
 */
public class ArregloDinamico implements IArregloDinamico {
	/**
	 * Capacidad maxima del arreglo
	 */
	private int tamanoMax;
	/**
	 * Numero de elementos en el arreglo (de forma compacta desde la posicion 0)
	 */
	private int tamanoAct;
	/**
	 * Arreglo de elementos de tamaNo maximo
	 */
	private VOMovingViolations elementos[];

	/**
	 * Construir un arreglo con la capacidad maxima inicial.
	 * 
	 * @param max
	 *            Capacidad maxima inicial
	 */
	public ArregloDinamico(int max) {
		elementos = new VOMovingViolations[max];
		tamanoMax = max;
		tamanoAct = 0;
	}

	public void agregar(VOMovingViolations dato) {
		if (tamanoAct == tamanoMax) { // caso de arreglo lleno (aumentar tamaNo)
			tamanoMax = 2 * tamanoMax;
			VOMovingViolations[] copia = elementos;
			elementos = new VOMovingViolations[tamanoMax];
			for (int i = 0; i < tamanoAct; i++) {
				elementos[i] = copia[i];
			}
			System.out.println("Arreglo lleno: " + tamanoAct + " - Arreglo duplicado: " + tamanoMax);
		}
		elementos[tamanoAct] = dato;
		tamanoAct++;
	}

	public int darTamano() {
		return tamanoAct;
	}

	public VOMovingViolations darElemento(int i) {
		return elementos[i];
	}

	public VOMovingViolations buscar(VOMovingViolations dato) {
		VOMovingViolations RTA = null;
		boolean encontrado = false;

		for (int i = 0; i < elementos.length && !encontrado; i++) {

			if (dato != null && elementos != null) {
				VOMovingViolations actual = elementos[i];
				if (actual.compareTo(dato) == 0) {
					RTA = actual;
					encontrado = true;
				}
			} else {
				encontrado = true;
			}
		}
		return RTA;
	}

	public VOMovingViolations eliminar(VOMovingViolations dato) {
		VOMovingViolations RTA = null;
		boolean encontrado = false;
		int indice = -1;

		for (int i = 0; i < elementos.length && !encontrado; i++) {
			VOMovingViolations actual = elementos[i];

			if (actual.compareTo(dato) == 0) {
				RTA = actual;
				encontrado = true;
			}
			indice++;

			for (int j = 0; j < elementos.length-1; j++) {
				elementos[j] = elementos[j + 1];
			}

			elementos[elementos.length - 1] = null;
			tamanoAct--;
			return RTA;
		}

		return RTA;
	}
	
	public VOMovingViolations[] darArreglo()
	{
		return elementos;
	}


}
