package data_structures;
	/**
	 * Abstract Data Type for a linked list of generic objects
	 * This ADT should contain the basic operations to manage a list
	 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
	 * next, previous
	 * @param <T>
	 */
	public interface ILinkedList<T> extends Iterable<T> {

		
		public void add(T elemento);
		
		public void addAtEnd(T elemento);
		
		public void addAtK(T elemento, int posicion);
		
		public T get(int i);
		
		public T getCurrentElement();
		
		Integer getSize();
		
		public void delete();
		
		public void deleteAtK(int K);
		
		public T next();
		
		public T previous();
				
	}

