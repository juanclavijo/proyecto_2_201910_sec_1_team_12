package logic;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.StringTokenizer;

import data_structures.IMaxHeapCP;
import data_structures.IQueue;
import data_structures.MaxHeapCP;
import data_structures.Queue;
import data_structures.RedBlackBalancedSearchTrees;
import data_structures.linkedList;


import vo.InfraccionesFecha;
import vo.InfraccionesFechaHora;
import vo.InfraccionesFranjaHoraria;
import vo.InfraccionesFranjaHorariaViolationCode;
import vo.InfraccionesLocalizacion;
import vo.InfraccionesViolationCode;
import vo.VOMovingViolations;


public class MovingViolationsManager 
{
	IQueue<VOMovingViolations> cola = new Queue<VOMovingViolations>();

	public int loadJanuary(String archivoJanuary) 
	{
		int c = 0;
		File archivo = new File(archivoJanuary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");

				StringTokenizer st = new StringTokenizer(linea, ",");

				LinkedList<String> datosActual = new LinkedList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}


				VOMovingViolations nuevoViolations = new VOMovingViolations(
						datosActual.get(0), datosActual.get(2), datosActual.get(3), datosActual.get(4),
						datosActual.get(5), datosActual.get(6), datosActual.get(7), datosActual.get(8),
						datosActual.get(9), datosActual.get(10), datosActual.get(11), datosActual.get(12),
						datosActual.get(13), datosActual.get(14), datosActual.get(15));

			//	l.add(nuevoViolations);
				cola.enqueue(nuevoViolations);
				c++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return c;
	}

	public int loadFebruary(String archivoFebruary) {
		int c = 0;
		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");

				StringTokenizer st = new StringTokenizer(linea, ",");

				LinkedList<String> datosActual = new LinkedList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}


				VOMovingViolations nuevoViolations = new VOMovingViolations(
						datosActual.get(0), datosActual.get(2), datosActual.get(3), datosActual.get(4),
						datosActual.get(5), datosActual.get(6), datosActual.get(7), datosActual.get(8),
						datosActual.get(9), datosActual.get(10), datosActual.get(11), datosActual.get(12),
						datosActual.get(13), datosActual.get(14), datosActual.get(15));

				//l.add(nuevoViolations);
				cola.enqueue(nuevoViolations);
				c++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return c;
	}

	public int loadMarch(String archivoFebruary) {
		int c = 0;
		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");

				StringTokenizer st = new StringTokenizer(linea, ",");

				LinkedList<String> datosActual = new LinkedList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}


				VOMovingViolations nuevoViolations = new VOMovingViolations(
						datosActual.get(0), datosActual.get(2), datosActual.get(3), datosActual.get(4),
						datosActual.get(5), datosActual.get(6), datosActual.get(7), datosActual.get(8),
						datosActual.get(9), datosActual.get(10), datosActual.get(11), datosActual.get(12),
						datosActual.get(13), datosActual.get(14), datosActual.get(15));

				//l.add(nuevoViolations);
				cola.enqueue(nuevoViolations);
				c++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return c;
	}

	public int loadApril(String archivoFebruary) {
		int c = 0;
		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");

				StringTokenizer st = new StringTokenizer(linea, ",");

				LinkedList<String> datosActual = new LinkedList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}


				VOMovingViolations nuevoViolations = new VOMovingViolations(
						datosActual.get(0), datosActual.get(2), datosActual.get(3), datosActual.get(4),
						datosActual.get(5), datosActual.get(6), datosActual.get(7), datosActual.get(8),
						datosActual.get(9), datosActual.get(10), datosActual.get(11), datosActual.get(12),
						datosActual.get(13), datosActual.get(14), datosActual.get(15));

			//	l.add(nuevoViolations);
				cola.enqueue(nuevoViolations);
				c++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return c;
	}

	public int loadMay(String archivoFebruary) {
		int c = 0;
		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");

				StringTokenizer st = new StringTokenizer(linea, ",");

				LinkedList<String> datosActual = new LinkedList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}


				VOMovingViolations nuevoViolations = new VOMovingViolations(
						datosActual.get(0), datosActual.get(2), datosActual.get(3), datosActual.get(4),
						datosActual.get(5), datosActual.get(6), datosActual.get(7), datosActual.get(8),
						datosActual.get(9), datosActual.get(10), datosActual.get(11), datosActual.get(12),
						datosActual.get(13), datosActual.get(14), datosActual.get(15));

				//l.add(nuevoViolations);
				cola.enqueue(nuevoViolations);
				c++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return c;
	}

	public int loadJune(String archivoFebruary) 
	{
		int c = 0;
		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");

				StringTokenizer st = new StringTokenizer(linea, ",");

				LinkedList<String> datosActual = new LinkedList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}


				VOMovingViolations nuevoViolations = new VOMovingViolations(
						datosActual.get(0), datosActual.get(2), datosActual.get(3), datosActual.get(4),
						datosActual.get(5), datosActual.get(6), datosActual.get(7), datosActual.get(8),
						datosActual.get(9), datosActual.get(10), datosActual.get(11), datosActual.get(12),
						datosActual.get(13), datosActual.get(14), datosActual.get(15));

				//l.add(nuevoViolations);
				cola.enqueue(nuevoViolations);
				c++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return c;

	}

	public int loadJuly(String archivoFebruary) {
		int c = 0;
		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");

				StringTokenizer st = new StringTokenizer(linea, ",");

				LinkedList<String> datosActual = new LinkedList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}


				VOMovingViolations nuevoViolations = new VOMovingViolations(
						datosActual.get(0), datosActual.get(2), datosActual.get(3), datosActual.get(4),
						datosActual.get(5), datosActual.get(6), datosActual.get(7), datosActual.get(8),
						datosActual.get(9), datosActual.get(10), datosActual.get(11), datosActual.get(12),
						datosActual.get(13), datosActual.get(14), datosActual.get(15));

				//l.add(nuevoViolations);
				cola.enqueue(nuevoViolations);
				c++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return c;

	}

	public int loadAugust(String archivoFebruary) {
		int c = 0;
		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");

				StringTokenizer st = new StringTokenizer(linea, ",");

				LinkedList<String> datosActual = new LinkedList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}


				VOMovingViolations nuevoViolations = new VOMovingViolations(
						datosActual.get(0), datosActual.get(2), datosActual.get(3), datosActual.get(4),
						datosActual.get(5), datosActual.get(6), datosActual.get(7), datosActual.get(8),
						datosActual.get(9), datosActual.get(10), datosActual.get(11), datosActual.get(12),
						datosActual.get(13), datosActual.get(14), datosActual.get(15));

				//
//l.add(nuevoViolations);
				cola.enqueue(nuevoViolations);
				c++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return c;

	}

	public int loadSeptember(String archivoFebruary) {
		int c = 0;
		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");

				StringTokenizer st = new StringTokenizer(linea, ",");

				LinkedList<String> datosActual = new LinkedList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}


				VOMovingViolations nuevoViolations = new VOMovingViolations(
						datosActual.get(0), datosActual.get(2), datosActual.get(3), datosActual.get(4),
						datosActual.get(5), datosActual.get(6), datosActual.get(7), datosActual.get(8),
						datosActual.get(9), datosActual.get(10), datosActual.get(11), datosActual.get(12),
						datosActual.get(13), datosActual.get(14), datosActual.get(15));

				//l.add(nuevoViolations);
				cola.enqueue(nuevoViolations);
				c++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return c;
	}

	public int loadOctober(String archivoFebruary) {
		int c = 0;
		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");

				StringTokenizer st = new StringTokenizer(linea, ",");

				LinkedList<String> datosActual = new LinkedList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}


				VOMovingViolations nuevoViolations = new VOMovingViolations(
						datosActual.get(0), datosActual.get(2), datosActual.get(3), datosActual.get(4),
						datosActual.get(5), datosActual.get(6), datosActual.get(7), datosActual.get(8),
						datosActual.get(9), datosActual.get(10), datosActual.get(11), datosActual.get(12),
						datosActual.get(13), datosActual.get(14), datosActual.get(15));

				//l.add(nuevoViolations);
				cola.enqueue(nuevoViolations);
				c++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return c;
	}

	public int loadNovember(String archivoFebruary) {
		int c = 0;
		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");

				StringTokenizer st = new StringTokenizer(linea, ",");

				LinkedList<String> datosActual = new LinkedList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}


				VOMovingViolations nuevoViolations = new VOMovingViolations(
						datosActual.get(0), datosActual.get(2), datosActual.get(3), datosActual.get(4),
						datosActual.get(5), datosActual.get(6), datosActual.get(7), datosActual.get(8),
						datosActual.get(9), datosActual.get(10), datosActual.get(11), datosActual.get(12),
						datosActual.get(13), datosActual.get(14), datosActual.get(15));

				//l.add(nuevoViolations);
				cola.enqueue(nuevoViolations);
				c++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return c;

	}

	public int loadDecember(String archivoFebruary) {
		int c = 0;
		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");

				StringTokenizer st = new StringTokenizer(linea, ",");

				LinkedList<String> datosActual = new LinkedList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}


				VOMovingViolations nuevoViolations = new VOMovingViolations(
						datosActual.get(0), datosActual.get(2), datosActual.get(3), datosActual.get(4),
						datosActual.get(5), datosActual.get(6), datosActual.get(7), datosActual.get(8),
						datosActual.get(9), datosActual.get(10), datosActual.get(11), datosActual.get(12),
						datosActual.get(13), datosActual.get(14), datosActual.get(15));

				//l.add(nuevoViolations);
				cola.enqueue(nuevoViolations);
				c++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return c;
	}


	
	public IQueue<VOMovingViolations> darListaColaMovingViolations()
	{
		return cola;
	}

	public IQueue<VOMovingViolations> darCopiaQueue()
	{
		IQueue<VOMovingViolations> copia = new Queue<VOMovingViolations>();

		for(VOMovingViolations s: cola)
		{
			copia.enqueue(s);;
		}
		return copia;
	}
	
}