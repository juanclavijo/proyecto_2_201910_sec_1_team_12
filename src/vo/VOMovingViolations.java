package vo;



/**
 * Representation of a Trip object
 */
public class VOMovingViolations implements Comparable<VOMovingViolations>
{
	/**
	 * representa el ID 
	 */
	private int objectID;	
	private String Location;	
	private double adressID;
	private int streetSEGID;
	private double xCOORD;
	private double yCOORD;
	private String ticketType;
	private int fineAMT;
	private double totalPaid;
	private int penalty1;
	private int penalty2;
	private String accidentIndicator;
	private String TicketIssueDate;
	private String violationCode;
	private String ViolationDesc;

	public VOMovingViolations(String pObjectID,String pLocation, String pAdressID, String pStreetSEGID, String pXCOORD,
			String pYCOORD, String pTicketType, String pFineAMT, String pTotalPaid, String pPenalty1, String pPenalty2, 
			String pAccidentIndicator, String pTicketIssueDate, String pViolationCode, String pViolationDesc) 
	{
		objectID = Integer.parseInt(pObjectID);
		Location = pLocation;
		adressID = Double.parseDouble(pAdressID);
		streetSEGID = Integer.parseInt(pStreetSEGID);
		xCOORD = Double.parseDouble(pXCOORD);
		yCOORD = Double.parseDouble(pYCOORD);
		ticketType = pTicketType;
		fineAMT = Integer.parseInt(pFineAMT);
		totalPaid = Math.round((Double.parseDouble(pTotalPaid)));
		penalty1 = Integer.parseInt(pPenalty1);
		penalty2 = Integer.parseInt(pPenalty2);
		accidentIndicator = pAccidentIndicator;
		TicketIssueDate = pTicketIssueDate;
		violationCode = pViolationCode;
		ViolationDesc = pViolationDesc;

	}

	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() 
	{
		return objectID;
	}

	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation()
	{
		return Location;
	}
	
	/**
	 * @return adressID 
	 */
	public double getAdressId()
	{
		return adressID;
	}
	
	public int getStreetSegid()
	{
		return streetSEGID;
	}
	
	public double getxCOORD() 
	{
		return xCOORD;
	}
	
	public double getyCOORD()
	{
		return yCOORD;
	}

	/**
	 * @return ticketType  .
	 */
	public String getTicketType() 
	{
		return ticketType;
	}
	
	public int getFineAMT()
	{
		return fineAMT;
	}

	public double getTotalPaid() 
	{
		return totalPaid;
	}
	
	public int getPenalty1() 
	{
		return penalty1;
	}
	
	public int getPenalty2()
	{
		return penalty2;
	}

	
	public String gerAccidentIndicator()
	{
		return accidentIndicator;
	}
	
	public String getTicketIssueDate()
	{
		return TicketIssueDate;
	}
	
	public String getViolationCode() 
	{
		return violationCode;
	}
	
	public String getViolationDesc()
	{
		return ViolationDesc;
	}

	public int compareTo(VOMovingViolations dato) {
		
		int RTA  =0;
		
		if (this.objectID < dato.objectID ) {
			RTA = -1; 
		}
		else if (this.objectID > dato.objectID) {
			RTA = 1;
		}
		return RTA;
		
	}
	
}
