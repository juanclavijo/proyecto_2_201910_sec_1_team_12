package vo;

import data_structures.IQueue;

/**
 * Agrupa las infracciones mostrando estadísticas sobre los datos 
 * como el total de infracciones que se presentan en ese conjunto,
 * el porcentaje de infracciones con y sin accidentes con respecto al total,
 * el valor total de las infracciones que se deben pagar y una lista con 
 * las infracciones. 
 */

public class EstadisticaInfracciones {

	@Override
	public String toString() {
		return "EstadisticaInfracciones [totalInfracciones=" + totalInfracciones + ",\n porcentajeAccidentes="
				+ porcentajeAccidentes + ",\n porcentajeNoAccidentes=" + porcentajeNoAccidentes + ",\n valorTotal="
				+ valorTotal + "]\n\n";
	}

	/**	
	 * Numero total de infraciones del conjunto
	 */

	protected int totalInfracciones;

	/**
	 * Porcentaje de las infracciones con accidentes con respecto al total
	 */

	protected double porcentajeAccidentes;

	/**
	 * Porcentaje de las infracciones sin accidentes con respecto al total
	 */

	protected double porcentajeNoAccidentes; 

	/**
	 * Valor total de las infracciones que se debe pagar.
	 */

	protected double valorTotal;	

	/**
	 * Lista con las infracciones que agrupa el conjunto
	 */

	protected IQueue<VOMovingViolations> listaInfracciones;


	/**
	 * Crea un nuevo conjunto con las infracciones
	 * @param listaInfracciones - Lista con las infracciones que cumplen el criterio de agrupamiento
	 */

	public EstadisticaInfracciones(IQueue<VOMovingViolations> lista) {
		this.listaInfracciones = lista;
		totalInfracciones = listaInfracciones.size();

		//TODO Hacer el calculo de porcentajeAccidentes, porcentajeNoAccidentes y valorTotal
		int conAccidente = 0;
		int sinAccidente = 0;
		int totalPagar = 0;
		for(VOMovingViolations s: lista)
		{
			if(s.gerAccidentIndicator().equalsIgnoreCase("Yes"))
				conAccidente++;
			else
			{ 
				sinAccidente++;
				totalPagar += s.getFineAMT();
			}
		}

		if(lista.size() > 0)
		{
			porcentajeAccidentes = (conAccidente*100)/(lista.size());   //TODO Calcular con base en la lista
			porcentajeNoAccidentes = (sinAccidente*100)/(lista.size()); //TODO Calcular con base en la lista
		}
		else
		{
			porcentajeAccidentes = 0;   
			porcentajeNoAccidentes = 0;
		}
		valorTotal = totalPagar;         //TODO Calcular con base en la lista
	}

	//=========================================================
	//Metodos Getters and Setters
	//=========================================================

	/**
	 * Gets the total infracciones.
	 * @return the total infracciones
	 */

	public int getTotalInfracciones() {
		return totalInfracciones;
	}	


	/**
	 * Gets the porcentaje accidentes.	 *
	 * @return the porcentaje accidentes
	 */

	public double getPorcentajeAccidentes() {
		//TODO Completar para que calcule el porcentaje de las infracciones del conjunto que sufrieron accidentes
		//con respecto al total.

		//		int con = 0;
		//		if(listaInfracciones != null)
		//		{
		//			for(VOMovingViolations s : listaInfracciones)
		//			{
		//				if(s.gerAccidentIndicator().equalsIgnoreCase("Yes"))
		//					con++;
		//			}
		//		}
		//		else System.out.println("lista nula");
		//		porcentajeAccidentes = (con*100)/(listaInfracciones.size()); 
		return porcentajeAccidentes;
	}	


	/**
	 * Gets the porcentaje no accidentes.
	 *
	 * @return the porcentaje no accidentes
	 */
	public double getPorcentajeNoAccidentes() {
		//TODO Completar para que calcule el porcentaje de las infracciones del conjunto que NO sufrieron accidentes
		//con respecto al total.

		//		int sin = 0;
		//		if(listaInfracciones != null)
		//		{
		//			for(VOMovingViolations s : listaInfracciones)
		//			{
		//				if(s.gerAccidentIndicator().equalsIgnoreCase("No"))
		//					sin++;
		//			}
		//		}
		//		else System.out.println("lista nula");
		//		porcentajeNoAccidentes = (sin*100)/(listaInfracciones.size()); 
		return porcentajeNoAccidentes;
	}

	/**
	 * Gets the valor total.
	 *
	 * @return the valor total
	 */
	public double getValorTotal() {
		//TODO Completar para calcular el valor total de dinero que representan las infracciones

		//		if(listaInfracciones != null)
		//		{
		//			for(VOMovingViolations s : listaInfracciones)
		//			{
		//				valorTotal += s.getFineAMT();
		//			}
		//		}
		//		else System.out.println("lista nula");

		return valorTotal;
	}	

	/**
	 * Gets the lista infracciones.
	 *
	 * @return the lista infracciones
	 */
	public IQueue<VOMovingViolations> getListaInfracciones() {
		return listaInfracciones;
	}

	/**
	 * Sets the lista infracciones.
	 *
	 * @param listaInfracciones the new lista infracciones
	 */

	public void setListaInfracciones(IQueue<VOMovingViolations> listaInfracciones) {
		this.listaInfracciones = listaInfracciones;
	}
}
